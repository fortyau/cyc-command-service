select
     et.ID
  , et.CHECK_NUMBER
  , et.JOURNAL_ID
  , et.TYPE_ID as PaymentTypeId -- use for Reimbursement Method
  , et.CREATE_DATE as PaidDate
  , et.SETTLE_DATE as ClearedDate
  , p.ID as ProviderID
  , p.NAME as ProviderName
  , p.ADDRESS1 as ProviderAddress1
  , p.ADDRESS2 as ProviderAddress2
  , p.CITY as ProviderCity
  , p.STATE as ProviderState
  , p.ZIPCODE as ProviderZipCode
  , et.AMOUNT * -1 as AMOUNT
  , ssl.Name as Status
  , et.ISSUE_DATE
  , et.REJECT_REASON
  , et.MEMO as NOTES
from b_externaltransfer et
join POSTING_JOURNAL pj on et.JOURNAL_ID = pj.jid
join B_PROVIDERINVOICE pi on pj.PINVOICE_ID = pi.INVOICE_ID
join b_provider p on pi.PROVIDER_ID = p.id
left join M_SIMPLELOOKUP ssl on et.STATUS_ID = ssl.ID
where et.id = :paymentId