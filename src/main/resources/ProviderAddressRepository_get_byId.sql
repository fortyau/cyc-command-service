select
  p.NAME
  , p.ADDRESS1
  , p.ADDRESS2
  , p.CITY
  , p.STATE
  , p.ZIPCODE
from b_provider p
where p.id = :providerId