SELECT
    c.submission_id                                     AS SUBMISSION_ID,
    c.id                                                AS CLAIM_ID,
    c.service_date                                      AS DATE_OF_SERVICE,
    c.cyc_created_time                                  AS CREATED_DATE,
    c.description                                       AS DESCRIPTION,
    c.receipt_date                                      AS RECEIPT_DATE,
    cst.name                                            AS SERVICE_TYPE,
    c.merchant_code                                     AS MCC_CODE,
    c.claim_amount                                      AS CLAIM_AMOUNT,
    ppl.service_for                                     AS SERVICE_FOR,
    cs.name                                             AS STATUS,
    crt.name                                            AS REJECT_TYPE,
    v1.void_reason                                      AS REJECT_REASON,
    sl.name                                             AS ORIGINATION_POINT,
    cst.name                                            AS CLAIM_TYPE,
    at.name                                             AS ACCOUNT_TYPE,
    CASE WHEN rc.id IS NOT NULL THEN 1 ELSE 0 END       AS RECURRING,
    c.vendor                                            AS VENDOR,
    e.id                                                AS EMPLOYEE_ID,
    p.first_name                                        AS EMPLOYEE_FIRST_NAME,
    p.last_name                                         AS EMPLOYEE_LAST_NAME,
    p.last_four_ssn                                     AS EMPLOYEE_SSN_LAST4,
    n.note_summary                                      AS NOTE_SUMMARY,
    n.note_text                                         AS NOTE_TEXT,
    i.amount                                            AS INVOICE_AMOUNT,
    (select abs(sum(p.AMOUNT))
    from B_CLAIM cx
      join B_POSTING p
        on cx.ID = p.INVOICE_ID
      join B_ACCOUNT a
        on p.ACCOUNT_ID = a.ID
      join B_JOURNAL j
        on p.JOURNAL_ID = j.id
    where cx.SUBMISSION_ID = :id
          and a.ACCOUNTTYPE_ID <> 21
          and j.SETTLE_STATUS_ID = 474
          and j.ACTIVITY_ID not in (612, 613))          AS REIMBURSED_AMOUNT
FROM b_claim c
          JOIN b_claimsubmission cs             ON cs.id                            = COALESCE(c.submission_id, -1)
	      JOIN m_simplelookup ctsl              ON ctsl.id                          = cs.claimtype_id
          LEFT JOIN b_claimstatus cs            ON COALESCE(c.status_id, -1)        = cs.id
          LEFT JOIN b_claimrejecttype crt       ON COALESCE(crt.id, -1)             = c.rejecttype_id
          LEFT JOIN b_claimservicetype cst      ON COALESCE(c.servicetype_id, -1)   = cst.id
          LEFT JOIN m_simplelookup sl           ON sl.id                            = COALESCE(c.origination, -1)
          LEFT JOIN m_employee e                ON e.id                             = COALESCE(c.employee_id, -1)
          LEFT JOIN m_person p                  ON p.id                             = e.id
          LEFT JOIN provider_payment_letter ppl ON ppl.claim_id                     = c.id
          LEFT JOIN b_account a                 ON a.id                             = COALESCE(c.account_id, -1)
          LEFT JOIN b_accounttype at            ON a.ACCOUNTTYPE_ID                 = at.id
          LEFT JOIN b_recurringclaim rc         ON c.id                             = rc.id
          LEFT JOIN m_note n                    ON n.id                             = COALESCE(c.internalnote_id, -1)
          LEFT JOIN (
                        SELECT p.invoice_id AS submission_id, et.reject_reason AS VOID_REASON
                        FROM b_posting p JOIN b_externaltransfer et
                                    ON et.journal_id   = p.journal_id
                        WHERE et.issue_type = 12412 -- Check Void
                        AND p.invoice_id IS NOT NULL
                        AND et.reject_reason IS NOT NULL
          ) v1 ON v1.submission_id = c.submission_id
        JOIN b_invoice i ON c.ID = i.ID
WHERE c.submission_id = :id