BEGIN
  update b_provider p
  set name = :name,
    address1 = :address1,
    address2 = :address2,
    city = :city,
    state = :state,
    zipcode = :zipCode
  where p.ID = :providerId;

  IF SQL%ROWCOUNT = 1 THEN
    COMMIT;
  ELSIF SQL%ROWCOUNT = 0 THEN
    ROLLBACK;
    RAISE_APPLICATION_ERROR(-20000, 'Provider not found');
  ELSE
    ROLLBACK;
    RAISE_APPLICATION_ERROR(-20001, 'Multiple providers found');
  END IF;
END;