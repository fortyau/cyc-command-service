select 
   et.ID
  , et.CHECK_NUMBER
  , et.JOURNAL_ID
  , et.TYPE_ID as PaymentTypeId -- use for Reimbursement Method
  , et.CREATE_DATE as PaidDate
  , et.SETTLE_DATE as ClearedDate
  , p.ID as ProviderID
  , p.NAME as ProviderName
  , p.ADDRESS1 as ProviderAddress1
  , p.ADDRESS2 as ProviderAddress2
  , p.CITY as ProviderCity
  , p.STATE as ProviderState
  , p.ZIPCODE as ProviderZipCode
  , et.AMOUNT * -1 AS AMOUNT
  , ssl.Name as Status
  , et.ISSUE_DATE
  , et.REJECT_REASON
  , et.MEMO as NOTES
from posting_journal pj
join posting_journal pj1 on pj1.journal_id = pj.journal_id and pj1.pid<> pj.pid
join posting_journal pj2 on pj2.pinvoice_id= pj1.pinvoice_id
join b_providerinvoice pi on pi.invoice_id= pj1.pinvoice_id
join b_externaltransfer et on et.journal_id= pj2.jid
join b_provider p on pi.PROVIDER_ID = p.id
left join M_SIMPLELOOKUP ssl on et.STATUS_ID = ssl.ID
join b_claim c on pj.pinvoice_id = c.id
where c.submission_id = :claimId
order by et.create_date desc