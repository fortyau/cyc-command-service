SELECT
        c.id                                        AS claim_id,
        c.claim_amount                              AS claim_amount,
        st.name                                     AS claim_status,
        c.authorization_date                        AS effective_date,
        p.first_name                                AS employee_first_name,
        p.last_name                                 AS employee_last_name,
        p.last_four_ssn                             AS employee_ssn_last_four,
        e.name                                      AS employer_name,
        sh.name                                     AS sponsor_name,
        at.name                                     AS account_type,
        sl.name                                     AS claim_type
FROM b_claim c JOIN b_claimsubmission cs     ON cs.id           = c.submission_id
               JOIN m_job j                  ON j.id            = cs.job_id
               JOIN m_person p               ON p.id            = j.employee_id
               JOIN b_claimstatus st         ON st.id           = c.status_id
               JOIN m_employer e             ON e.id            = j.employer_id
               JOIN m_hierarchy eh           ON e.hierarchy_id  = eh.id
               JOIN m_hierarchy dh           ON dh.id           = eh.parent_id
               JOIN m_hierarchy sh           ON sh.id           = dh.parent_id
               JOIN m_simplelookup sl        ON cs.claimtype_id = sl.id
               JOIN b_account acc            ON c.account_id    = acc.id
               JOIN b_accounttype at         ON at.id           = acc.accounttype_id
WHERE p.last_four_ssn IN ( :values )
AND c.status_id != 1                     -- Not Submitted
AND c.servicetype_id NOT IN ( 168, 169 ) -- NOT Add an Authorized User OR Replace A Card

