SELECT s.ID                     AS SPONSOR_ID,
       s.NAME                   AS SPONSOR_NAME,
       s.HIERARCHY_TYPE         AS SPONSOR_HIERARCHY,
       d.ID                     AS DISTRIBUTOR_ID,
       d.NAME                   AS DISTRIBUTOR_NAME,
       d.HIERARCHY_TYPE         AS DISTRIBUTOR_HIERARCHY,
       e.ID                     AS EMPLOYER_ID,
       e.NAME                   AS EMPLOYER_NAME,
       e.HIERARCHY_TYPE         AS EMPLOYER_HIERARCHY,
       est.NAME			        AS EMPLOYER_STATUS,
       e.CREATE_DATE            AS EMPLOYER_CREATE_DATE,
       me.TAX_ID                AS EMPLOYER_TAX_ID,
       epy.PLAN_YEAR_IDENTIFIER AS EMPLOYER_PLAN_YEAR
FROM  M_HIERARCHY e JOIN M_HIERARCHY d			           ON e.PARENT_ID = d.ID
                    JOIN M_HIERARCHY s			           ON d.PARENT_ID = s.ID
                    JOIN M_EMPLOYER me                     ON me.ID       = e.ID
                    JOIN M_EMPLOYERSTATUS es	           ON e.ID        = es.EMPLOYER_ID
                    JOIN M_EMPLOYERSTATUSTYPE est          ON es.STATUS   = est.ID
                    LEFT JOIN M_EMPLOYEROFFERING eo        ON e.ID        = eo.EMPLOYER_ID
                    LEFT JOIN EMPLOYER_SETUP_PLAN_YEAR epy ON epy.ID      = eo.EMPLOYERPLANYEAR_ID
WHERE UPPER(s.NAME || ' ' || d.NAME || ' ' || e.NAME) LIKE '%' || UPPER( :searchText ) || '%'