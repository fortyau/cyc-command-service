package com.connectyourcare.command.repository;

import com.connectyourcare.command.dto.EmployerDTO;
import com.connectyourcare.command.repository.mapper.EmployerSearchResultRowMapper;
import com.sun.media.sound.InvalidDataException;
import javassist.NotFoundException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@Repository
public class EmployerRepository extends BaseRepository {
	public EmployerDTO getEmployerById(BigDecimal id) throws NotFoundException, InvalidDataException, IOException {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", id);

		List results = getJdbcTemplate().query(
				getResource("EmployerRepository_getEmployerByID.sql"),
				parameters,
				new EmployerSearchResultRowMapper());

		EmployerDTO employer;

		if(results.size() == 1) {
            employer = (EmployerDTO) results.get(0);
        } else if (results.size() == 0) {
		    throw new NotFoundException(String.format("EmployerDTO %s not found.", id));
        } else {
            throw new InvalidDataException(String.format("More than one employer returned for ID %s", id));
        }

        return employer;
	}
}
