package com.connectyourcare.command.repository;

import com.connectyourcare.command.repository.exceptions.NotFoundException;
import com.connectyourcare.command.repository.exceptions.UnknownDataException;
import java.sql.SQLException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.support.SQLExceptionTranslator;

public class ProviderAddressExceptionTranslator implements SQLExceptionTranslator{
    @Override
    public DataAccessException translate(String task, String sql, SQLException ex) {
        switch(ex.getErrorCode()){
            case(20000):
                return new NotFoundException("Provider not found");
            case (20001):
                throw new IllegalStateException();
            default:
                return new UnknownDataException(ex.getMessage());
        }
    }
}
