package com.connectyourcare.command.repository;

import com.connectyourcare.command.dto.ClaimDTO;
import com.connectyourcare.command.repository.mapper.ClaimDetailRowMapper;
import com.connectyourcare.command.repository.mapper.ClaimSearchResultRowMapper;
import com.sun.media.sound.InvalidDataException;
import javassist.NotFoundException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.InvalidParameterException;
import java.util.List;

@Repository
public class ClaimRepository extends BaseRepository {
    public List findClaims(String field, List<String> values) throws IOException, InvalidParameterException {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("values", values);

        String resourceFile = null;

        if(field.equalsIgnoreCase("claimid")) {
            resourceFile = "ClaimRepository_findClaims_ByID.sql";
        } else if(field.equalsIgnoreCase("ssn")) {
            resourceFile = "ClaimRepository_findClaims_BySSN.sql";
        }

        if(resourceFile == null) {
            return null;
        } else {
            return getJdbcTemplate().query(getResource(resourceFile),
                parameters,
                new ClaimSearchResultRowMapper());
        }
    }

    public ClaimDTO getClaimById(BigDecimal id) throws NotFoundException, IOException {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("id", id);

        List claims = getJdbcTemplate().query(getResource("ClaimRepository_getClaimDetails_ByID.sql"),
                parameters,
                new ClaimDetailRowMapper());

        ClaimDTO claim;

        if(claims.size() == 1) {
            claim = (ClaimDTO) claims.get(0);
        } else if (claims.size() == 0) {
            throw new NotFoundException(String.format("ClaimDTO %s not found.", id));
        } else {
            throw new InvalidDataException(String.format("More than one claim returned for ID %s", id));
        }

        return claim;
    }
    
    public List getClaimsRelatedToPayment(int paymentId) throws IOException {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("paymentId", paymentId);

        String resourceFile = "ClaimRepository_findClaims_ByPaymentId.sql";

        return getJdbcTemplate().query(getResource(resourceFile),
            parameters,
            new ClaimSearchResultRowMapper());
    }
}