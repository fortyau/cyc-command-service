package com.connectyourcare.command.repository;

import com.connectyourcare.command.dto.ProviderAddressUpdateDTO;
import com.connectyourcare.command.repository.mapper.PaymentResultRowMapper;
import com.connectyourcare.command.repository.mapper.ProviderAddressUpdateDTORowMapper;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class ProviderAddressRepository extends BaseRepository {
    public void update(int providerId, ProviderAddressUpdateDTO address) throws IOException {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("providerId", providerId);
        parameters.addValue("name", address.getName());
        parameters.addValue("address1", address.getAddress1());
        parameters.addValue("address2", address.getAddress2());
        parameters.addValue("city", address.getCity());
        parameters.addValue("state", address.getState());
        parameters.addValue("zipCode", formatZipCode(address.getZipCode()));
        
        NamedParameterJdbcTemplate template = getJdbcTemplate();
        template.getJdbcTemplate().setExceptionTranslator(new ProviderAddressExceptionTranslator());
        template.update(getResource("ProviderAddressRepository_update.sql"), parameters);
    }
    
    private String formatZipCode(String zipCode) {
        return zipCode.replace("-", "");
    }
    
    public Object get(int providerId) throws IOException {
        if (!(providerId >= 1))
            throw new InvalidParameterException("providerId");

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("providerId", providerId);

        String resourceFile = "ProviderAddressRepository_get_byId.sql";

        List list = getJdbcTemplate().query(getResource(resourceFile),
                parameters,
                new ProviderAddressUpdateDTORowMapper());
        
        switch (list.size()) {
            case 0:
                return null;
            case 1:
                return list.get(0);
            default:
                throw new IllegalStateException("More than one provider found");
        }
    }
}
