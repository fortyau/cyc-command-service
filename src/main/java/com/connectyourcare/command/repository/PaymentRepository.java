package com.connectyourcare.command.repository;

import com.connectyourcare.command.repository.mapper.PaymentResultRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.List;

@Repository
public class PaymentRepository extends BaseRepository {
    public List findPayments(int claimId) throws IOException, InvalidParameterException {
        if (!(claimId >= 1))
            throw new InvalidParameterException("claimId");

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("claimId", claimId);

        String resourceFile = "PaymentRepository_findPayments_byClaimId.sql";

        return getJdbcTemplate().query(getResource(resourceFile),
                parameters,
                new PaymentResultRowMapper());
    }
    
    public Object getPayment(int paymentId) throws IOException, InvalidParameterException, IllegalStateException {
        if (!(paymentId >= 1))
            throw new InvalidParameterException("paymentId");

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("paymentId", paymentId);

        String resourceFile = "PaymentRepository_findPayment_byId.sql";

        List list = getJdbcTemplate().query(getResource(resourceFile),
                parameters,
                new PaymentResultRowMapper());
        
        switch (list.size()) {
            case 0:
                return null;
            case 1:
                return list.get(0);
            default:
                throw new IllegalStateException("More than one payment found");
        }
    }
}
