package com.connectyourcare.command.repository.exceptions;

import org.springframework.dao.DataAccessException;

public class UnknownDataException extends DataAccessException {
    public UnknownDataException(String msg) {
        super(msg);
    }    
}
