package com.connectyourcare.command.repository.exceptions;

import org.springframework.dao.DataAccessException;

public class NotFoundException extends DataAccessException {
    public NotFoundException(String msg) {
        super(msg);
    }
}
