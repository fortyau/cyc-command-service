package com.connectyourcare.command.repository.mapper;

import com.connectyourcare.command.dto.ProviderAddressUpdateDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProviderAddressUpdateDTORowMapper implements RowMapper<Object> {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        ProviderAddressUpdateDTO dto = new ProviderAddressUpdateDTO();
        dto.setName(rs.getString("NAME"));
        dto.setAddress1(rs.getString("ADDRESS1"));
        dto.setAddress2(rs.getString("ADDRESS2"));
        dto.setCity(rs.getString("CITY"));
        dto.setState(rs.getString("STATE"));
        dto.setZipCode(rs.getString("ZIPCODE"));
        return dto;
    }
}