package com.connectyourcare.command.repository.mapper;

import com.connectyourcare.command.dto.ClaimDTO;
import com.connectyourcare.command.dto.EmployeeDTO;
import com.connectyourcare.command.dto.NoteDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClaimDetailRowMapper implements RowMapper<Object> {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new ClaimDTO(
                        rs.getBigDecimal("submission_id"),
                        rs.getBigDecimal("claim_id"),
                        rs.getDate("date_of_service"),
                        rs.getDate("created_date"),
                        rs.getString("description"),
                        rs.getDate("receipt_date"),
                        rs.getString("service_type"),
                        rs.getString("mcc_code"),
                        rs.getFloat("claim_amount"),
                        rs.getFloat("reimbursed_amount"),
                        rs.getString("service_for"),
                        rs.getString("status"),
                        rs.getString("reject_type"),
                        rs.getString("reject_reason"),
                        rs.getString("origination_point"),
                        rs.getString("claim_type"),
                        rs.getString("account_type"),
                        rs.getBoolean("recurring"),
                        rs.getString("vendor"),
                        new EmployeeDTO(
                                rs.getBigDecimal("employee_id"),
                                rs.getString("employee_first_name"),
                                rs.getString("employee_last_name"),
                                rs.getString("employee_ssn_last4")
                        ),
                        new NoteDTO(
                            rs.getString("note_summary"),
                            rs.getString("note_text")
                        ),
                        rs.getBigDecimal("invoice_amount")
		);
	}
}