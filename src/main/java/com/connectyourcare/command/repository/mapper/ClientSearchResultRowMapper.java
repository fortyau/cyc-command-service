package com.connectyourcare.command.repository.mapper;

import com.connectyourcare.command.dto.ClientSearchResultDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientSearchResultRowMapper implements RowMapper<Object> {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new ClientSearchResultDTO(
				rs.getBigDecimal("sponsor_id"),
				rs.getString("sponsor_name"),
				rs.getBigDecimal("sponsor_hierarchy"),
				rs.getBigDecimal("distributor_id"),
				rs.getString("distributor_name"),
				rs.getBigDecimal("distributor_hierarchy"),
				rs.getBigDecimal("employer_id"),
				rs.getString("employer_name"),
				rs.getBigDecimal("employer_hierarchy"),
				rs.getString("employer_status"),
				rs.getDate("employer_create_date"),
				rs.getString("employer_tax_id"),
				rs.getString("employer_plan_year")
		);
	}
}