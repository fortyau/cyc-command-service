package com.connectyourcare.command.repository.mapper;

import com.connectyourcare.command.dto.ClaimSearchResultDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClaimSearchResultRowMapper implements RowMapper<Object> {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new ClaimSearchResultDTO(
                        rs.getBigDecimal("submission_id"),
                        rs.getBigDecimal("claim_id"),
                        rs.getString("claim_status"),
                        rs.getFloat("claim_amount"),
                        rs.getString("claim_type"),
                        rs.getString("account_type"),
                        rs.getDate("service_date"),
                        rs.getString("employee_first_name"),
                        rs.getString("employee_last_name"),
                        rs.getString("employee_ssn_last_four"),
                        rs.getString("employer_name"),
                        rs.getString("sponsor_name"),
                        rs.getString("vendor"),
                        rs.getBigDecimal("invoice_amount")
		);
	}
}