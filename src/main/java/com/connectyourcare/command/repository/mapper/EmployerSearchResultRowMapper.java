package com.connectyourcare.command.repository.mapper;

import com.connectyourcare.command.dto.EmployerDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployerSearchResultRowMapper implements RowMapper<Object> {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new EmployerDTO(
			rs.getBigDecimal("id"),
                rs.getDate("create_date"),
                rs.getString("name"),
                rs.getString("logo"),
                rs.getString("default_reimbursement_method"),
                rs.getString("initial_username_format"),
                rs.getString("initial_password_format"),
                rs.getDate("file_processing_start_date"),
                rs.getString("broker_name"),
                rs.getString("default_multi_purse")
		);
	}
}