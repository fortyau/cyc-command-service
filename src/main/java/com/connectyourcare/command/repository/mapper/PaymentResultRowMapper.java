package com.connectyourcare.command.repository.mapper;

import com.connectyourcare.command.dto.PaymentDTO;
import com.connectyourcare.command.dto.ProviderDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PaymentResultRowMapper implements RowMapper<Object> {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new PaymentDTO(
            rs.getInt("ID"),
            rs.getString("CHECK_NUMBER"),
            rs.getInt("JOURNAL_ID"),
            rs.getInt("PaymentTypeId"),
            rs.getDate("PaidDate"),
            rs.getDate("ClearedDate"),
            new ProviderDTO(
                rs.getBigDecimal("ProviderId"),
                rs.getString("ProviderName"), 
                rs.getString("ProviderAddress1"),
                rs.getString("ProviderAddress2"),
                rs.getString("ProviderCity"),
                rs.getString("ProviderState"),
                rs.getString("ProviderZipCode")
            ),
            rs.getBigDecimal("AMOUNT"),
            rs.getString("Status"),
            rs.getDate("ISSUE_DATE"),
            rs.getString("REJECT_REASON"),
            rs.getString("NOTES")
        );
    }
}