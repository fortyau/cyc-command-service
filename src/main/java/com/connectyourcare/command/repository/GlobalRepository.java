package com.connectyourcare.command.repository;

import com.connectyourcare.command.repository.mapper.ClientSearchResultRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.List;

@Repository
public class GlobalRepository extends BaseRepository {
	public List findClients(String searchText) throws IOException {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("searchText", searchText);

		return getJdbcTemplate().query(
				getResource("GlobalRepository_findClients.sql"),
				parameters,
				new ClientSearchResultRowMapper());
	}
}
