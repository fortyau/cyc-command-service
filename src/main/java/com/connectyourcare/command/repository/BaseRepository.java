package com.connectyourcare.command.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.io.*;

@Repository
@SuppressWarnings("unused")
public class BaseRepository {
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    protected NamedParameterJdbcTemplate getJdbcTemplate() {
        return this.jdbcTemplate;
    }

    protected String getResource(String resourceFile) throws IOException {
        String resourceContents;
        Resource resource = new ClassPathResource(resourceFile);
        String contents = "";

        InputStream is = resource.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        char[] c = new char[(int) resource.contentLength()];
        if (br.read(c, 0, c.length) > 0) {
            return new String(c);
        }

        br.close();
        return contents;
    }
}
