package com.connectyourcare.command.dto;

public class NoteDTO {
    private String summary;
    private String detail;

    public NoteDTO(String summary, String detail) {
        this.summary = summary;
        this.detail = detail;
    }

    public String getSummary() { return this.summary; }
    public String getDetail() { return this.detail; }
}
