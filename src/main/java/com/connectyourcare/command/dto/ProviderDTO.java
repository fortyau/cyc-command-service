package com.connectyourcare.command.dto;

import java.math.BigDecimal;

@SuppressWarnings("unused")
public final class ProviderDTO {
    private BigDecimal id;
    private String name;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zipCode;
    
    public ProviderDTO(BigDecimal id, String name, String address1, String address2, String city, String state, String zipCode) {
        this.id = id;
        this.name = name;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
    }
    
    public BigDecimal getId() { return id; }
    public String getName() { return name; }
    public String getAddress1() { return address1; }
    public String getAddress2() { return address2; }
    public String getCity() { return city; }
    public String getState() { return state; }
    public String getZipCode() { return zipCode; }
}
