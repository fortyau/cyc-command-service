package com.connectyourcare.command.dto;

import java.util.List;

public class ErrorsResult {
    private List<String> errors;
    
    public ErrorsResult(List<String> errors) {
        this.errors = errors;
    }
    
    public List<String> getErrors() {
        return errors;
    }
}
