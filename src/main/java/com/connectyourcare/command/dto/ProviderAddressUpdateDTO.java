package com.connectyourcare.command.dto;

import java.util.ArrayList;

public class ProviderAddressUpdateDTO {
    private String name;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zipCode;
    
    public String getName() { return name; }
    public String getAddress1() { return address1; }
    public String getAddress2() { return address2; }
    public String getCity() { return city; }
    public String getState() { return state; }
    public String getZipCode() { return zipCode; }
    
    public void setName(String val) { this.name = val; }
    public void setAddress1(String val) { this.address1 = val; }
    public void setAddress2(String val) { this.address2 = val; }
    public void setCity(String val) { this.city = val; }
    public void setState(String val) { this.state = val; }
    public void setZipCode(String val) { this.zipCode = val; }
    
    public void validate() throws ValidationException {
        ArrayList<String> errors = new ArrayList<>();
        
        if (getName() == null || getName().isEmpty()) {
            errors.add("Name is required");
        } else if (getName().length() > 50) {
            errors.add("Name cannot be longer than 50 characters");
        }
        
        if (getAddress1() == null || getAddress1().isEmpty()) {
            errors.add("Address1 is required");
        } else if (getAddress1().length() > 40) {
            errors.add("Address1 cannot be longer than 40 characters");
        }
        
        if (getAddress2() != null && getAddress2().length() > 40) {
            errors.add("Address2 cannot be longer than 40 characters");
        }
        
        if (getCity() == null || getCity().isEmpty()) {
            errors.add("City is required");
        } else if (getCity().length() > 20) {
            errors.add("City cannot be longer than 20 characters");
        }
        
        if (getState() == null || getState().isEmpty()) {
            errors.add("State is required");
        } else if (!getState().matches("^(?:A[KLRZ]|C[AOT]|D[CE]|FL|GA|HI|I[ADLN]|K[SY]|LA|M[ADEINOST]|N[CDEHJMVY]|O[HKR]|PA|RI|S[CD]|T[NX]|UT|V[AT]|W[AIVY])*$")) {
            errors.add("State must be the two character code for a US State");
        }
        
        if (getZipCode() == null || getZipCode().isEmpty()) {
            errors.add("ZipCode is required");
        } else if (!getZipCode().matches("^([0-9]{5})-?(?:[0-9]{4})?$")) {
            errors.add("ZipCode must be 5 or 9 digits");
        }
        
        if (errors.size() > 0) {
            throw new ValidationException(errors);
        }
    }
}
