package com.connectyourcare.command.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

public final class EmployerDTO {
	private BigDecimal id;
	private Date createdDate;
	private String name;
	private String logo;
	private String defaultReimbursementMethod;
	private String initialUsernameFormat;
	private String initialPasswordFormat;
	private Date fileProcessingStartDate;
	private String brokerName;
	private String defaultMultiPurse;

	public EmployerDTO(BigDecimal id, Date createdDate, String name, String logo, String defaultReimbursementMethod,
					   String initialUsernameFormat, String initialPasswordFormat, Date fileProcessingStartDate,
					   String brokerName, String defaultMultiPurse) {
	    this.id  = id;
	    this.createdDate = createdDate;
	    this.name = name;
	    this.logo = logo;
	    this.defaultReimbursementMethod = defaultReimbursementMethod;
	    this.initialUsernameFormat = initialUsernameFormat;
	    this.initialPasswordFormat = initialPasswordFormat;
	    this.fileProcessingStartDate = fileProcessingStartDate;
	    this.brokerName = brokerName;
	    this.defaultMultiPurse = defaultMultiPurse;
    }

    @ApiModelProperty(notes = "The employer ID")
	public BigDecimal getId() { return this.id; }
	public String getName() { return this.name; }
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
	public Date getCreatedDate() { return this.createdDate; }
	public String getLogo() { return this.logo; }
	public String getDefaultReimbursementMethod() { return this.defaultReimbursementMethod; }
	public String getInitialUsernameFormat() { return this.initialUsernameFormat; }
	public String getInitialPasswordFormat() { return this.initialPasswordFormat; }
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
	public Date getFileProcessingStartDate() { return this.fileProcessingStartDate; }
	public String getBrokerName() { return this.brokerName; }
	public String getDefaultMultiPurse() { return this.defaultMultiPurse; }
}
