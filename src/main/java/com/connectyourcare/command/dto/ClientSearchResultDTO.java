package com.connectyourcare.command.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

@SuppressWarnings("unused")
public final class ClientSearchResultDTO {
	private BigDecimal sponsorId;
	private String sponsorName;
	private BigDecimal sponsorHierarchy;
	private BigDecimal distributorId;
	private String distributorName;
	private BigDecimal distributorHierarchy;
	private BigDecimal employerId;
	private String employerName;
	private BigDecimal employerHierarchy;
	private String employerStatus;
	private Date employerCreateDate;
	private String employerTaxId;
	private String employerPlanYear;

	public ClientSearchResultDTO(BigDecimal sponsorId, String sponsorName, BigDecimal sponsorHierarchy,
								 BigDecimal distributorId, String distributorName, BigDecimal distributorHierarchy,
								 BigDecimal employerId, String employerName, BigDecimal employerHierarchy,
								 String employerStatus, Date employerCreateDate, String employerTaxId,
								 String employerPlanYear) {
		this.sponsorId = sponsorId;
		this.sponsorName = sponsorName;
		this.sponsorHierarchy = sponsorHierarchy;
		this.distributorId = distributorId;
		this.distributorName = distributorName;
		this.distributorHierarchy = distributorHierarchy;
		this.employerId = employerId;
		this.employerName = employerName;
		this.employerHierarchy = employerHierarchy;
		this.employerStatus = employerStatus;
		this.employerCreateDate = employerCreateDate;
		this.employerTaxId = employerTaxId;
		this.employerPlanYear = employerPlanYear;
	}

	public BigDecimal getSponsorId() { return this.sponsorId; }
	public String getSponsorName()  { return this.sponsorName; }
	public BigDecimal getSponsorHierarchy() { return this.sponsorHierarchy; }
	public BigDecimal getDistributorId() { return this.distributorId; }
	public String getDistributorName()  { return this.distributorName; }
	public BigDecimal getDistributorHierarchy() { return this.distributorHierarchy; }
	public BigDecimal getEmployerId() { return this.employerId; }
	public String getEmployerName()  { return this.employerName; }
	public BigDecimal getEmployerHierarchy() { return this.employerHierarchy; }
	public String getEmployerStatus() { return this.employerStatus; }
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
	public Date getEmployerCreateDate() { return this.employerCreateDate; }
	public String getEmployerTaxId() { return this.employerTaxId; }
	public String getEmployerPlanYear() { return this.employerPlanYear; }
}
