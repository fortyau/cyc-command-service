package com.connectyourcare.command.dto;

import java.util.List;

public class ValidationException extends Exception {
    private List<String> messages;
    
    public ValidationException(List<String> messages) {
        this.messages = messages;
    }
    
    public List<String> getMessages() {
        return messages;
    }
}
