package com.connectyourcare.command.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@SuppressWarnings("unused")
@JsonPropertyOrder({
        "submissionId",
        "claimId",
        "dateOfService",
        "dateCreated" ,
        "receiptDate",
        "amount",
        "reimbursedAmount",
        "notReimbursedAmount",
        "description",
        "mccCode",
        "serviceFor",
        "status",
        "rejectType",
        "originationPoint",
        "serviceType",
        "claimType",
        "accountType",
        "recurring",
        "vendor",
        "employee",
        //"provider",
        "note"
})
public final class ClaimDTO {
    @NotNull
    private BigDecimal submissionId;
    @NotNull
    private BigDecimal claimId;
    @NotNull
    private Date dateOfService;
    @NotNull
    private Date dateCreated;
    private String description;
    private Date receiptDate;
    private String serviceType;
    private String mmcCode;
    @NotNull
    private float amount;
    private float reimbursedAmount;
    private String serviceFor;
    private String status;
    private String rejectType;
    private String rejectReason;
    private String originationPoint;
    @NotNull
    private EmployeeDTO employee;
    private String claimType;
    @NotNull
    private String accountType;
    @NotNull
    private boolean recurring;
    private NoteDTO note;
    private String vendor;
    private BigDecimal invoiceAmount;

    public ClaimDTO(BigDecimal submissionId, BigDecimal claimId, Date dateOfService, Date dateCreated, String description,
                    Date receiptDate, String serviceType, String mmcCode, float amount, float reimbursedAmount,
                    String serviceFor, String status, String rejectType, String rejectReason,
                    String originationPoint, String claimType, String accountType, boolean recurring, String vendor,
                    EmployeeDTO employee, NoteDTO note, BigDecimal invoiceAmount) {
        this.submissionId = submissionId;
        this.claimId = claimId;
        this.dateOfService = dateOfService;
        this.dateCreated = dateCreated;
        this.description = description;
        this.receiptDate = receiptDate;
        this.serviceType = serviceType;
        this.mmcCode = mmcCode;
        this.amount = amount;
        this.reimbursedAmount = reimbursedAmount;
        this.serviceFor = serviceFor;
        this.status = status;
        this.rejectType = rejectType;
        this.rejectReason = rejectReason;
        this.originationPoint = originationPoint;
        this.employee = employee;
        this.claimType = claimType;
        this.accountType = accountType;
        this.recurring = recurring;
        this.note = note;
        this.vendor = vendor;
        this.invoiceAmount = invoiceAmount;
    }

    public BigDecimal getSubmissionId() { return this.submissionId; }
    public BigDecimal getClaimId() { return this.claimId; }
    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date getDateOfService() { return this.dateOfService; }
    @JsonFormat(pattern = "yyyy-MM-dd") 
    public Date getDateCreated() { return this.dateCreated; }
    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date getReceiptDate() { return this.receiptDate; }
    public float getAmount() { return this.amount; }
    public float getReimbursedAmount() { return this.reimbursedAmount; }
    public float getNotReimbursedAmount() { 
        return getAmount() - getReimbursedAmount();
    }
    public String getDescription() { return this.description; }
    public String getMccCode() { return this.mmcCode; }
    public String getServiceFor() { return this.serviceFor; }
    public String getStatus() { return this.status; }
    public String getRejectType() { return this.rejectType; }
    public String getRejectReason() { return this.rejectReason; }
    public String getOriginationPoint() { return this.originationPoint; }
    public String getServiceType() { return this.serviceType; }
    public String getClaimType() { return this.claimType; }
    public String getAccountType() { return this.accountType; }
    public boolean getRecurring() { return this.recurring; }
    public String getVendor() { return this.vendor; }
    public EmployeeDTO getEmployee() { return this.employee; }
    public NoteDTO getNote() { return this.note; }
    public BigDecimal getInvoiceAmount() { return this.invoiceAmount; }
}
