package com.connectyourcare.command.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@SuppressWarnings("unused")
public final class ClaimSearchResultDTO {
        @NotNull
	private BigDecimal submissionId;	
        @NotNull
	private BigDecimal claimId;
	@NotNull
	private String claimStatus;
	@NotNull
	private float claimAmount;
	@NotNull
	private Date serviceDate;
	@NotNull
	private String employeeFirstName;
	@NotNull
	private String employeeLastName;
	@NotNull
	private String employeeSsnLastFour;
	@NotNull
	private String employerName;
	@NotNull
	private String sponsorName;
	@NotNull
	private String accountType;
	@NotNull
	private String claimType;
        private String vendor;
        private BigDecimal invoiceAmount;

	public ClaimSearchResultDTO(BigDecimal submissionId, BigDecimal claimId, String claimStatus, float claimAmount, String claimType,
								String accountType, Date serviceDate, String employeeFirstName, String employeeLastName,
								String employeeSsnLastFour, String employerName, String sponsorName, String vendor,
                                                                BigDecimal invoiceAmount) {
		this.submissionId = submissionId;
                this.claimId = claimId;
		this.claimStatus = claimStatus;
		this.claimAmount = claimAmount;
		this.serviceDate = serviceDate;
		this.employeeFirstName = employeeFirstName;
		this.employeeLastName = employeeLastName;
		this.employeeSsnLastFour = employeeSsnLastFour;
		this.employerName = employerName;
		this.sponsorName = sponsorName;
		this.claimType = claimType;
		this.accountType = accountType;
                this.vendor = vendor;
                this.invoiceAmount = invoiceAmount;
	}

        public BigDecimal getSubmissionId() { return this.submissionId; }
	public BigDecimal getClaimId() { return this.claimId; }
	public String getClaimStatus() { return this.claimStatus; }
	public float getClaimAmount() { return this.claimAmount; }
	public String getClaimType() { return this.claimType; }
	public String getAccountType() { return this.accountType; }
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getServiceDate() { return this.serviceDate; }
	public String getEmployeeLastName() { return this.employeeLastName; }
	public String getEmployeeFirstName() { return this.employeeFirstName; }
	public String getEmployeeSsnLastFour() { return this.employeeSsnLastFour; }
	public String getEmployerName() { return this.employerName; }
	public String getSponsorName() { return this.sponsorName; }
        public String getVendor() { return this.vendor; }
        public BigDecimal getInvoiceAmount() { return this.invoiceAmount; }
}
