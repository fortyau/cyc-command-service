package com.connectyourcare.command.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigDecimal;

import java.util.Date;

@SuppressWarnings("unused")
public final class PaymentDTO {
    private int id;
    private String checkNumber;
    private int journalId;
    private int typeId;
    private Date paidDate;
    private Date clearedDate;
    private ProviderDTO provider;
    private BigDecimal amount;
    private String status;
    private Date issueDate;
    private String rejectReason;
    private String notes;
    
    public PaymentDTO(int id, String checkNumber, int journalId, int typeId, Date paidDate, Date clearedDate, 
            ProviderDTO provider, BigDecimal amount, String status, Date issueDate, String rejectReason, String notes) {
        this.id = id;
        this.checkNumber = checkNumber;
        this.journalId = journalId;
        this.typeId = typeId;
        this.paidDate = paidDate;
        this.clearedDate = clearedDate;
        this.provider = provider;
        this.amount = amount;
        this.status = status;
        this.issueDate = issueDate;
        this.rejectReason = rejectReason;
        this.notes = notes;
    }
    
    public int getId() { return id; }
    public String getCheckNumber() { return checkNumber; }
    public int getJournalId() { return journalId; }
    public int getTypeId() { return typeId; }
    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date getPaidDate() { return paidDate; }
    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date getClearedDate() { return clearedDate; }
    public ProviderDTO getProvider() { return provider; }
    public BigDecimal getAmount() { return amount; }
    public String getStatus() { return status; }
    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date getIssueDate() { return issueDate; }
    public String getRejectReason() { return rejectReason; }
    public String getNotes() { return notes; }
}