package com.connectyourcare.command.dto;

import java.math.BigDecimal;

public class EmployeeDTO {
    private BigDecimal id;
    private String firstName;
    private String lastName;
    private String ssnLast4;

    public EmployeeDTO(BigDecimal id, String firstName, String lastName, String ssnLast4) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.ssnLast4 = ssnLast4;
    }

    public BigDecimal getId() { return this.id; }
    public String getFirstName() { return this.firstName; }
    public String getLastName() { return this.lastName; }
    public String getSsnLast4() { return this.ssnLast4; }
}
