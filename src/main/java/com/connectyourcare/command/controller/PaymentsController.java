package com.connectyourcare.command.controller;

import com.connectyourcare.command.service.PaymentService;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.InvalidParameterException;

@RestController
@Api(value="Payment Service", description="Operations pertaining to payments.")
@SuppressWarnings("unused")
public class PaymentsController {
	private PaymentService paymentService;

	public PaymentsController(PaymentService paymentService) {
		this.paymentService = paymentService;
	}

	@ApiOperation(value = "Get payments for a claim")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 400, message = "Invalid search parameters"),
            @ApiResponse(code = 500, message = "Server error"),
	})
	@ResponseBody
        @CrossOrigin(origins = "*")
        @RequestMapping(method = RequestMethod.GET,
                value = "/claims/{claimId}/payments",
                produces = {MediaType.APPLICATION_JSON_VALUE})
	public Object getPaymentsForClaim(@PathVariable(value = "claimId", required = true)int claimId) {
		try {
			return this.paymentService.getPaymentsByClaimId(claimId);
		} catch(InvalidParameterException ipe) {
			// TODO Add exception handling
			System.out.println(ipe.getMessage());
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		} catch(IOException ioe) {
            // TODO Add exception handling
			System.out.println(ioe.getMessage());
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
        
        @ApiOperation(value = "Get payment by id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 400, message = "Invalid search parameters"),
            @ApiResponse(code = 500, message = "Server error"),
	})
	@ResponseBody
        @CrossOrigin(origins = "*")
        @RequestMapping(method = RequestMethod.GET,
                value = "/payments/{id}",
                produces = {MediaType.APPLICATION_JSON_VALUE})
	public Object getPayment(@PathVariable(value = "id", required = true)int id) throws IOException {
            Object result = this.paymentService.getPayment(id);
            
            if (result == null) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
            
            return result;
	}
}
