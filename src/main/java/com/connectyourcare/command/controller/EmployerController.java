package com.connectyourcare.command.controller;

import com.connectyourcare.command.service.EmployerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.math.BigDecimal;

@RestController
@Api(value="EmployerDTO Service", description="Operations pertaining to employers.")
@RequestMapping("/employers")
@SuppressWarnings("unused")
public class EmployerController {
	private EmployerService service;

	public EmployerController(EmployerService service) {
		this.service = service;
	}

    @ApiOperation(value = "Get details for a single employer")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Request executed successfully"),
            @ApiResponse(code = 404, message = "The requested employer was not found"),
            @ApiResponse(code = 500, message = "An internal error occurred or bad data found")
    })
	@RequestMapping(
			method = RequestMethod.GET,
			value = "/{id}",
			produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
	public Object GetEmployerById(@PathVariable(required = true) BigDecimal id) {
		try {
            return this.service.getEmployerById(id);
		} catch(NotFoundException nfe) {
			// TODO Add exception handling
			return new ResponseEntity(HttpStatus.NOT_FOUND);
        } catch (IOException ioe) {
			// TODO Add exception handling
			System.out.println(ioe.getMessage());
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
