package com.connectyourcare.command.controller;

import com.connectyourcare.command.service.option.ClaimSearchOptions;
import com.connectyourcare.command.service.ClaimService;
import io.swagger.annotations.*;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.InvalidParameterException;

@RestController
@Api(value="ClaimDTO Service", description="Operations pertaining to claims.")
@SuppressWarnings("unused")
public class ClaimController {
	private ClaimService claimService;

	public ClaimController(ClaimService claimService) {
		this.claimService = claimService;
	}

	@ApiOperation(value = "Search for claims")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 400, message = "Invalid search parameters"),
            @ApiResponse(code = 500, message = "Server error"),
	})
	@ApiParam(examples = @Example(value = {
			@ExampleProperty(
					mediaType="application/json",
					value = "{ \"searchField\": \"claimid\", \"values\": [ \"123456\",\"789012\" ]}"
			)
	}))
	@RequestMapping(
			method = RequestMethod.POST,
			value = "claims/search",
			produces = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
        @CrossOrigin(origins = "*")
	public Object searchClaims(@RequestBody(required = true) ClaimSearchOptions options) {
		try {
		    options.validate();

			return this.claimService.findClaims(options);
		} catch(InvalidParameterException ipe) {
			// TODO Add exception handling
			System.out.println(ipe.getMessage());
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		} catch(IOException ioe) {
            // TODO Add exception handling
			System.out.println(ioe.getMessage());
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiOperation(value = "Get a claim by ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully retrieved a claim"),
            @ApiResponse(code = 404, message = "The requested claim was not found"),
			@ApiResponse(code = 500, message = "Server error"),
	})
    @RequestMapping(
            method = RequestMethod.GET,
            value = "claims/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
        @CrossOrigin(origins = "*")
	public Object getClaimById(@PathVariable(required = true) BigDecimal id) {
		try {
            return this.claimService.getClaimById(id);
        } catch(NotFoundException nfe) {
            // TODO Add exception handling
            return new ResponseEntity(HttpStatus.NOT_FOUND);
		} catch(IOException ioe) {
			// TODO Add exception handling
			System.out.println(ioe.getMessage());
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
        
        @RequestMapping(
            method = RequestMethod.GET,
            value = "/payments/{paymentId}/related-claims",
            produces = {MediaType.APPLICATION_JSON_VALUE})
        @CrossOrigin(origins = "*")
        public Object getClaimsRelatedToPayment(@PathVariable int paymentId) throws IOException {
            return this.claimService.getClaimsRelatedToPayment(paymentId);
        }
}
