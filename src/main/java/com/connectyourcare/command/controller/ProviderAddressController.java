package com.connectyourcare.command.controller;

import com.connectyourcare.command.dto.ErrorsResult;
import com.connectyourcare.command.service.ProviderService;
import com.connectyourcare.command.dto.ProviderAddressUpdateDTO;
import com.connectyourcare.command.dto.ValidationException;
import com.connectyourcare.command.repository.exceptions.NotFoundException;
import io.swagger.annotations.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import org.springframework.http.HttpStatus;

@RestController
@Api
@SuppressWarnings("unused")
public class ProviderAddressController {
        private ProviderService providerService;
    
	public ProviderAddressController(ProviderService providerService) { 
            this.providerService = providerService;
        }

	@ApiOperation(value = "Update Provider Address")
	@ResponseBody
        @CrossOrigin(origins = "*")
        @RequestMapping(method = RequestMethod.PUT,
                value = "/providers/{providerId}/address",
                produces = {MediaType.APPLICATION_JSON_VALUE})
	public Object updateProviderAddress(@PathVariable(value = "providerId", required = true)int providerId, @RequestBody(required = true) ProviderAddressUpdateDTO address) throws IOException {
            try {
                return this.providerService.updateAddress(providerId, address);	    
            }
            catch (ValidationException ex) {
                return new ResponseEntity(new ErrorsResult(ex.getMessages()), HttpStatus.BAD_REQUEST);
            }
            catch (NotFoundException ex) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
	}
}
