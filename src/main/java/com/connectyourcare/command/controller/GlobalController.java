package com.connectyourcare.command.controller;

import com.connectyourcare.command.service.option.ClientSearchOptions;
import com.connectyourcare.command.service.GlobalService;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.InvalidParameterException;

@RestController
@Api(value="Global Service", description="Operations that access multiple resource such as claims and employers.")
@RequestMapping("/global")
@SuppressWarnings("unused")
public class GlobalController {
	private GlobalService service;

	public GlobalController(GlobalService service) {
		this.service = service;
	}

	@ApiOperation(value = "Search for distributors, sponsors and employers")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully retrieved list"),
	})
	@ApiParam(examples = @Example(value = {
			@ExampleProperty(
					mediaType="application/json",
					value = "{\"searchText\": \"MyEmployer Lake Vista\", \"MatchType\": \"AnyWord\" }"
			)
	}))
	@RequestMapping(
			method = RequestMethod.POST,
			value = "/clients/search",
			produces = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public Object searchClients(@RequestBody(required = true) ClientSearchOptions options) {
		try {
			options.validate();

			return this.service.findClients(options);
		} catch(InvalidParameterException ipe) {
            // TODO Add exception handling
            System.out.println(ipe.getMessage());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
		} catch(IOException ioe) {
			// TODO Add exception handling
			System.out.println(ioe.getMessage());
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
