package com.connectyourcare.command.service;

import com.connectyourcare.command.dto.ProviderAddressUpdateDTO;
import com.connectyourcare.command.dto.ValidationException;
import com.connectyourcare.command.repository.ProviderAddressRepository;
import java.io.IOException;
import org.springframework.stereotype.Service;

@Service
public class ProviderService {
    private ProviderAddressRepository repo;

    public ProviderService(ProviderAddressRepository repo) {
        this.repo = repo;
    }

    public Object updateAddress(int providerId, ProviderAddressUpdateDTO address) throws IOException, ValidationException {
        address.validate();
        this.repo.update(providerId, address);    
        return this.repo.get(providerId);
    }
}
