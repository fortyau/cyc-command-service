package com.connectyourcare.command.service;

import com.connectyourcare.command.dto.EmployerDTO;
import com.connectyourcare.command.repository.EmployerRepository;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;

@Service
public class EmployerService {
	private EmployerRepository repo;

	public EmployerService(EmployerRepository repo) {
		this.repo = repo;
	}

	public EmployerDTO getEmployerById(BigDecimal id) throws NotFoundException, IOException {
		return this.repo.getEmployerById(id);
	}
}
