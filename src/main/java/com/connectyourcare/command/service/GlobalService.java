package com.connectyourcare.command.service;

import com.connectyourcare.command.service.option.ClientSearchOptions;
import com.connectyourcare.command.repository.GlobalRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class GlobalService {
	private GlobalRepository repo;

	public GlobalService(GlobalRepository repo) {
		this.repo = repo;
	}

	public List findClients(ClientSearchOptions options) throws IOException {
		return this.repo.findClients(options.getSearchText());
	}
}
