package com.connectyourcare.command.service.option;

import javax.validation.constraints.NotNull;
import java.security.InvalidParameterException;
import java.util.List;

public final class ClaimSearchOptions {
	@NotNull
	private List<String> searchValues;
	@NotNull
	private String searchField;

	public void setSearchValues(List<String> values) {
		this.searchValues = values;
	}

	public List<String> getSearchValues() {
		return this.searchValues;
	}

	public void setSearchField(String field) { this.searchField = field; }

	public String getSearchField() { return this.searchField; }

	public void validate() throws InvalidParameterException {
		if(!this.searchField.equalsIgnoreCase("claimid") && !this.searchField.equalsIgnoreCase("ssn")) {
			throw new InvalidParameterException(String.format("Invalid searchField: %s", this.searchField));
		}

		if(this.searchValues.size() == 0) {
			throw new InvalidParameterException("searchValues cannot be empty");
		}
	}
}
