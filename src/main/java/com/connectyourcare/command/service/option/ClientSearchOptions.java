package com.connectyourcare.command.service.option;

import javax.validation.constraints.NotNull;
import java.security.InvalidParameterException;

public final class ClientSearchOptions {
	private final int MIN_TEXT_LENGTH = 2;

	@NotNull
	private String searchText;
	@NotNull
	private String matchType;

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public String getSearchText() {
		String searchText = this.searchText;

		if(this.matchType.equalsIgnoreCase("AnyWord")) {
			searchText = searchText.replace(" ", "%");
		}

		return searchText;
	}

	public void setMatchType(String matchType) {
		this.matchType = matchType;
	}

	public void validate() throws InvalidParameterException {
		if(!this.matchType.equalsIgnoreCase("anyword") && !this.matchType.equalsIgnoreCase("exactmatch")) {
			throw new InvalidParameterException(String.format("Invalid matchType: %s", this.matchType));
		}

		if(this.searchText.trim().length() == 0) {
			throw new InvalidParameterException("searchText cannot be empty");
		}

		if(this.searchText.length() <= MIN_TEXT_LENGTH) {
			throw new InvalidParameterException(String.format("searchText must be at least %s charaters", MIN_TEXT_LENGTH));
		}
	}
}
