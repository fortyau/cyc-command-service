package com.connectyourcare.command.service;

import com.connectyourcare.command.dto.ClaimDTO;
import com.connectyourcare.command.service.option.ClaimSearchOptions;
import com.connectyourcare.command.repository.ClaimRepository;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@Service
public class ClaimService {
	private ClaimRepository repo;

	public ClaimService(ClaimRepository repo) {
		this.repo = repo;
	}

	public List findClaims(ClaimSearchOptions options) throws IOException {
		return this.repo.findClaims(options.getSearchField(), options.getSearchValues());
	}

	public ClaimDTO getClaimById(BigDecimal id) throws NotFoundException, IOException {
		return this.repo.getClaimById(id);
	}
        
        public List getClaimsRelatedToPayment(int paymentId) throws IOException {
            return this.repo.getClaimsRelatedToPayment(paymentId);
        }
}
