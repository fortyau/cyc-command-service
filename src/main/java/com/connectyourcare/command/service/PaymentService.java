package com.connectyourcare.command.service;

import com.connectyourcare.command.repository.PaymentRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.List;

@Service
public class PaymentService {
    private PaymentRepository repo;

    public PaymentService(PaymentRepository repo) {
        this.repo = repo;
    }

    public List getPaymentsByClaimId(int claimId) throws IOException {
        return this.repo.findPayments(claimId);
    }
    
    public Object getPayment(int paymentId) throws IOException, InvalidParameterException {
        return this.repo.getPayment(paymentId);
    }
}
