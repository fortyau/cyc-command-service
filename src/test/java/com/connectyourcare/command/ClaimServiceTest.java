package com.connectyourcare.command;

import com.connectyourcare.command.service.option.ClaimSearchOptions;
import com.connectyourcare.command.dto.ClaimSearchResultDTO;
import com.connectyourcare.command.repository.ClaimRepository;
import com.connectyourcare.command.service.ClaimService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Fail.fail;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
public class ClaimServiceTest {

	private ClaimRepository mockRepo;
	private List<ClaimSearchResultDTO> claimData;

	@Before
	public void Setup() {
		this.claimData = Arrays.asList(
				new ClaimSearchResultDTO(
                                                new BigDecimal(1869211),
						new BigDecimal(1869278),
						"Closed. No longer administered.",
						5.15f,
						"HSA",
						"Electronic ClaimDTO",
						new Date(1388556000),
						"John",
						"Doe",
						"1006",
						"EmployerDTO 1869278",
						"Sponsor 1869278",
                                                "Vendor 934287",
                                                new BigDecimal(20)),
				new ClaimSearchResultDTO(
                                                new BigDecimal(1869211),
						new BigDecimal(2154172),
						"Approved, processing claim payment",
						30.63f, "HSA",
						"Electronic ClaimDTO",
						new Date(1425448800),
						"Ben005193",
						"Smith005193",
						"5193",
						"EmployerDTO 2154172",
						"Sponsor 2154172",
                                                "Vendor 934287",
                                                new BigDecimal(20)),
				new ClaimSearchResultDTO(
                                                new BigDecimal(1869211),
						new BigDecimal(2246180),
						"Closed. No longer administered.",
						5.26f,
						"HSA",
						"Electronic ClaimDTO",
						new Date(1401253200),
						"Cherry",
						"Hall",
						"1234",
						"EmployerDTO 2246180",
						"Sponsor 2246180",
                                                "Vendor 934287",
                                                new BigDecimal(20)),
				new ClaimSearchResultDTO(
                                                new BigDecimal(1869211),
						new BigDecimal(2227585),
						"Loaded Awaiting Action",
						5.99f,
						"HSA",
						"Electronic ClaimDTO",
						new Date(1390975200),
						"Bob",
						"Dylan",
						"5678",
						"EmployerDTO 2227585",
						"Sponsor 2227585",
                                                "Vendor 934287",
                                                new BigDecimal(20))
		);

		this.mockRepo = mock(ClaimRepository.class);
	}

	private List<ClaimSearchResultDTO> initMockRepo_ClaimID(ClaimSearchOptions options) throws IOException {
		List<ClaimSearchResultDTO> list = this.claimData.stream()
				.filter(c -> options.getSearchValues().contains(c.getClaimId().toString()))
				.collect(Collectors.toList());
		when(this.mockRepo.findClaims(options.getSearchField(), options.getSearchValues())).thenReturn(list);
		return list;
	}

    private List<ClaimSearchResultDTO> initMockRepo_SSN(ClaimSearchOptions options) throws IOException {
        List<ClaimSearchResultDTO> list = this.claimData.stream()
                .filter(c -> options.getSearchValues().contains(c.getEmployeeSsnLastFour()))
                .collect(Collectors.toList());
        when(this.mockRepo.findClaims(options.getSearchField(), options.getSearchValues())).thenReturn(list);
        return list;
    }

	@Test
	public void searchSingleClaim_ByID() {
		try {
			ClaimSearchOptions options = new ClaimSearchOptions();
			options.setSearchValues(Arrays.asList("1869278"));
            options.setSearchField("claimid");

			List<ClaimSearchResultDTO> expected = initMockRepo_ClaimID(options);

			ClaimService service = new ClaimService(this.mockRepo);
			List actual = service.findClaims(options);

			assertEquals(1, actual.size());
			assertEquals(expected, actual);
		} catch(Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void searchMultipleClaims_ByID() {
		try {
			ClaimSearchOptions options = new ClaimSearchOptions();
			options.setSearchValues(Arrays.asList("1869278", "2154172"));
            options.setSearchField("claimid");

			List<ClaimSearchResultDTO> expected = initMockRepo_ClaimID(options);

			ClaimService service = new ClaimService(this.mockRepo);
			List actual = service.findClaims(options);

			assertEquals(2, actual.size());
			assertEquals(expected, actual);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void searchClaim_IDNotFound() {
		try {
			ClaimSearchOptions options = new ClaimSearchOptions();
			options.setSearchValues(Arrays.asList("9999999"));
            options.setSearchField("claimid");

			List<ClaimSearchResultDTO> expected = initMockRepo_ClaimID(options);

			ClaimService service = new ClaimService(this.mockRepo);
			List actual = service.findClaims(options);

			assertEquals(0, actual.size());
			assertEquals(expected, actual);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

    @Test
    public void searchSingleClaim_BySSN() {
        try {
            ClaimSearchOptions options = new ClaimSearchOptions();
            options.setSearchValues(Arrays.asList("1234"));
            options.setSearchField("ssn");

            List<ClaimSearchResultDTO> expected = initMockRepo_SSN(options);

            ClaimService service = new ClaimService(this.mockRepo);
            List actual = service.findClaims(options);

            assertEquals(1, actual.size());
            assertEquals(expected, actual);
        } catch(Exception e) {
            fail(e.getMessage());
        }
    }

	@Test
	public void searchMultipleClaims_BySSN() {
		try {
			ClaimSearchOptions options = new ClaimSearchOptions();
			options.setSearchValues(Arrays.asList("1234", "5193"));
			options.setSearchField("ssn");

			List<ClaimSearchResultDTO> expected = initMockRepo_SSN(options);

			ClaimService service = new ClaimService(this.mockRepo);
			List actual = service.findClaims(options);

			assertEquals(2, actual.size());
			assertEquals(expected, actual);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void searchClaim_SSNNotFound() {
		try {
			ClaimSearchOptions options = new ClaimSearchOptions();
			options.setSearchValues(Arrays.asList("9999"));
			options.setSearchField("ssn");

			List<ClaimSearchResultDTO> expected = initMockRepo_SSN(options);

			ClaimService service = new ClaimService(this.mockRepo);
			List actual = service.findClaims(options);

			assertEquals(0, actual.size());
			assertEquals(expected, actual);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}
