#!/bin/bash

mvn install:install-file -Dfile=$1 -DgroupId=com.oracle -DartifactId=ojdbc7 -Dversion=12.1.0.1 -Dpackaging=jar
