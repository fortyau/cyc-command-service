#!/bin/bash

ECR_REPO="cyc-command"
VERSION="latest"
ACCOUNT="380757016349"
EC2_NAME="cyc-cmd-svc"
EC2_USER="ec2-user"
EC2_IP=
CMD=
STAGE=
STAGE_NUM=1

function pipeline() {
    stage_build_jar
    stage_build_image
    stage_push_image
    stage_deploy
}

function stage_build_jar() {
    STAGE="Test & Build Jar" && output_stage
    cd .. && mvn clean install
    cd - && cp ../target/cyccommand-*SNAPSHOT.jar ./service.jar
}

function stage_build_image() {
    STAGE="Build Image" && output_stage
    docker build -t $ECR_REPO .
    rm service.jar
    docker tag $ECR_REPO:$VERSION $ACCOUNT.dkr.ecr.us-east-1.amazonaws.com/$ECR_REPO:$VERSION
}

function stage_push_image() {
    STAGE="Push Image" && output_stage
    $(aws ecr get-login --no-include-email)
    docker push $ACCOUNT.dkr.ecr.us-east-1.amazonaws.com/$ECR_REPO:$VERSION
}

function stage_deploy() {
    STAGE="Deploy" && output_stage
    container_id=$(CMD="docker ps|grep $ECR_REPO" && exec_ec2 | awk '{print $1}')
    image_id=$(CMD="docker images|grep $ECR_REPO" && exec_ec2 | awk '{print $3}')

    if [ "$container_id" != "" ]; then
        CMD="docker stop $container_id" && exec_ec2
        CMD="docker system prune --all --volumes --force" && exec_ec2
    fi

    CMD='$(aws ecr get-login --no-include-email --region us-east-1)' && exec_ec2
    CMD='docker-compose up -d' && exec_ec2

}

function exec_ec2() {
    if [ "$EC2_IP" == "" ]; then
        EC2_IP=$(aws ec2 describe-instances \
                --filters "Name=tag:Name,Values=$EC2_NAME" \
                --query 'Reservations[].Instances[].PrivateIpAddress' \
                --output=text)
    fi

    ssh $EC2_USER@$EC2_IP $CMD
}

function output_stage() {
    echo
    echo "------------------------------------ [ STAGE $STAGE_NUM: $STAGE ] ------------------------------------"
    echo

    STAGE_NUM=$(expr $STAGE_NUM + 1)
}

pipeline
