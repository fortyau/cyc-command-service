#!/bin/bash -e

function main() {
    install_docker
    upgrade_java
}

function upgrade_java() {
    yum remove java-1.7.0-openjdk -y
    yum install java-1.8.0 -y
}

function install_docker() {
    yum update -y
    yum install -y docker
    service docker start
    usermod -a -G docker ec2-user
    curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
}

main $@

